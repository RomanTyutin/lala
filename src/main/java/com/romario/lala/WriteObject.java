package com.romario.lala;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class WriteObject {
    public static void main(String[] args) {
        La first = new La(1, "John");
        La second = new La(2, "Ien");

        La[] laaaa = {new La(1, "John"), new La(2, "Ien"), new La(3, "Itan")};

        try(FileOutputStream fos = new FileOutputStream("la.bin");
            ObjectOutputStream oos = new ObjectOutputStream(fos)){
            oos.writeInt(laaaa.length);

            for(La la:laaaa){
                oos.writeObject(la);
            }


        }catch (FileNotFoundException e){
            System.err.println("File is not found!!!");
        }catch (IOException e){
            System.err.println("Input/Output error!!!");
        }
    }
}
