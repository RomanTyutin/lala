package com.romario.lala;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReadObject {
    public static void main(String[] args) {
        File file = new File("la.bin");
        La[] laList = null;
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))){
            int arrayLength = ois.readInt();
            laList = new La[arrayLength];
            for(int i = 0; i<arrayLength; i++){
                laList[i] = (La) ois.readObject();
            }
//            int i = 0;
//            while(true) {
//                laList[i] = (La) ois.readObject();
//                i++;
//            }
        }catch (EOFException e){
            System.out.println("End of file.");
        }
        catch (FileNotFoundException e){
            System.err.println("File is not found!!!");
        }catch (IOException e){
            System.err.println("Input/Output Error!!!");
        }catch (ClassNotFoundException e){
            System.err.println("Object's class is not found!!!");
        }


        System.out.println(Arrays.toString(laList));
        System.out.println(laList.length);
    }
}
