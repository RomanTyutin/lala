package com.romario.lala;

import java.io.Serializable;
import java.util.Objects;

public class La implements Serializable {

    private int id;
    private String name;

    public static final long serialVersionUID = 2L;

    public La(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString(){
        return id + " - " + name;
    }
}
